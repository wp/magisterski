package vp.magisterski.service.impl;

import org.springframework.stereotype.Service;
import vp.magisterski.service.DocumentService;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;
import java.util.Map;
import fr.opensagres.poi.xwpf.converter.pdf.PdfOptions;
import fr.opensagres.poi.xwpf.converter.pdf.PdfConverter;

@Service
public class DocumentServiceImpl implements DocumentService {
    @Override
    public byte[] generatePdfFromWordTemplate(String templatePath,  Map<String, String> placeholders) throws Exception {
        XWPFDocument document = loadWordTemplate(templatePath);

        for (Map.Entry<String, String> entry : placeholders.entrySet()) {
            replaceText(document, entry.getKey(), entry.getValue());
        }

        return convertToPdf(document);
    }

    public XWPFDocument loadWordTemplate(String templatePath) throws Exception {
        FileInputStream fis = new FileInputStream(templatePath);
        XWPFDocument document = new XWPFDocument(fis);
        fis.close();
        return document;
    }

    public void replaceText(XWPFDocument document, String placeholder, String replacement) {
        document.getParagraphs().forEach(paragraph -> {
            String text = paragraph.getText();
            if (text.contains(placeholder)) {
                text = text.replace(placeholder, replacement);
                String finalText = text;
                paragraph.getRuns().forEach(run -> {
                    run.setText(finalText, 0);
                });
            }
        });
    }

    public byte[] convertToPdf(XWPFDocument wordDocument) throws Exception {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            PdfOptions options = PdfOptions.create();
            PdfConverter.getInstance().convert(wordDocument, out, options);
            return out.toByteArray();
        } catch (IOException ex) {
            throw new RuntimeException("Error converting document to PDF", ex);
        }
    }


}
