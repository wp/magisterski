package vp.magisterski.service;

import java.util.Map;

public interface DocumentService {
    public byte[] generatePdfFromWordTemplate(String templatePath, Map<String, String> placeholders) throws Exception;
}
