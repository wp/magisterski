package vp.magisterski.service.scheduler;


import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import vp.magisterski.service.MasterThesisService;
import vp.magisterski.service.MasterThesisStatusChangeService;

@Service
public class MasterThesisScheduler {
    private final MasterThesisStatusChangeService masterThesisStatusChangeService;
    private final MasterThesisService masterThesisService;

    public MasterThesisScheduler(MasterThesisStatusChangeService masterThesisStatusChangeService, MasterThesisService masterThesisService) {
        this.masterThesisStatusChangeService = masterThesisStatusChangeService;
        this.masterThesisService = masterThesisService;
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void checkAndApproveStatuses(){
        masterThesisStatusChangeService.approveAllStatusesThatIsNotApprovedMoreThanFiveDays();
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void checkAndCancelOverdueTheses(){
        masterThesisService.checkAndCancelOverdueTheses();
    }


}
